package fr.inouk.OpenERPJSONRPCClient;
/*
Copyright (c) 2013 Cyril MORISSE ( @cmorisse )

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-- MIT License (MIT)
*/

import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: cmorisse
 * Date: 13/10/13
 * Time: 12:22
 * To change this template use File | Settings | File Templates.
 */
public class OpenERPJSONRPCClientException extends RuntimeException {
    public int code;
    public String message;
    public JSONObject data;
    public JSONObject jsonResponse;

    public OpenERPJSONRPCClientException(JSONObject jsonResponse) {
        this.jsonResponse = jsonResponse;
        this.message = (String) jsonResponse.getJSONObject("error").get("message");
        this.data = jsonResponse.getJSONObject("error").getJSONObject("data");
        this.code = jsonResponse.getJSONObject("error").getInt("code");
    }
}
